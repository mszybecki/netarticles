<?php

namespace Addition;

class ViewModel
{
    public $priority = 2;

    public $controller;
    public $moduleManager;

    public function run()
    {
        $this->controller->model->load('category');

        $this->controller->viewModel->categories = $this->controller->category->getAll();
        $this->controller->viewModel->action = $this->moduleManager->action;

        if ($this->controller->session->check("logged", true)) {
            $this->controller->viewModel->nick  = $this->controller->session->nick;
        }

        $this->controller->viewModel->cart = $this->controller->session->cart ?? array('length' => 0, 'products' => null);
        $this->controller->session->cart = $this->controller->viewModel->cart;
        $this->controller->session->save();

        $this->controller->model->unload();
    }
}
