<?php

namespace Addition
{
	class Model
	{
        public $priority = 1;
        public $controller;
	    public $moduleManager;

        private $connection;
        private $loaded = [];

        public function run()
        {
            $this->controller->model = $this;
        }

        public function load(string $model)
        {
            if ( empty($this->connection) )
            {
                $this->connection = new \PDO('mysql:host=localhost;dbname=netarticles', 'root', '');
                $this->connection->query("SET NAMES utf8");
            }

            $tmp = "\\Model\\$model";
			$tmp = new $tmp();
			$tmp->connection = $this->connection;

            $this->controller->{strtolower($model)} = $tmp;
            $this->loaded[] = $model;
        }

        public function unload()
        {
            foreach ( $this->loaded as $l )
                $this->controller->{$l} = null;

            $this->connection = null;
        }
	}
}
