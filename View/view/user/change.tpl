<main class="user">
    <div class="container">
        <div>
            <p>Change {$what}</p>

            {if isset($change)}
                {if $change}
                    <p>{$what} was change</p>
                {else}
                    <p>Something went wrong, try again</p>
                {/if}
            {/if}


            {if $what == "email"}
                <form method="post" action="{$path}change/email">
                    <label for="email">New email:</label>
                    {if !empty($error->email)}<p>{$error->email}</p>{/if}
                    <input type="email" name="email" id="email" />
                    <label for="password">Confirm your password:</label>
                    {if !empty($error->password)}<p>{$error->password}</p>{/if}
                    <input type="password" name="password" id="password" />
                    <input type="submit" value="Change email" />
                </form>
            {elseif $what == "password"}
                <form method="post" action="{$path}change/password">
                    <label for="old_password">Old password:</label>
                    {if !empty($error->old_password)}<p>{$error->old_password}</p>{/if}
                    <input type="password" name="old_password" id="old_password" />
                    <label for="new_password">New password:</label>
                    {if !empty($error->new_password)}<p>{$error->new_password}</p>{/if}
                    <input type="password" name="new_password" id="new_password" />
                    <label for="new_repeat_password">Repeat new password:</label>
                    {if !empty($error->new_repeat_password)}<p>{$error->new_repeat_password}</p>{/if}
                    <input type="password" name="new_repeat_password" id="new_repeat_password" />
                    <input type="submit" value="Change password" />
                </form>
            {/if}
        </div>
    </div>
</main>