<main class="user">
    <div class="container">
        <div>
            <p>Hello <strong>{$nick}</strong></p>
            <p><a href="{$path}change/password">Change password</a></p>
            <p><a href="{$path}change/email">Change email</a></p>
            <p><a href="{$path}logout">Logout</a></p>
            <p><a href="{$path}signout">Delete account</a></p>
            {if $type == "admin" || $type == "supervisior" }
                <p><a href="{$path}panel">Administrator's panel</a></p>
            {/if}
        </div>
    </div>
</main>
