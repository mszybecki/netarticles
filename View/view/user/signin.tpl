<main class="user">
    <div class="container">
        <div>
        {assign var=form value=true}
        {if isset($signin)}
            {if $signin == true}
                <p>You are signed in, you can log in into our webiste</p>
                {assign var=form value=false}
            {else}
                <p>Something went wrong</p>
            {/if}
        {/if}

        {if $form}
        <form method="post" action="{$path}signin">
            <label for="nick">Nick:</label>
            {if !empty($error->nick)}<p>{$error->nick}</p>{/if}
            <input type="text" name="nick" id="nick" />
            <label for="password">Password:</label>
            {if !empty($error->password)}<p>{$error->password}</p>{/if}
            <input type="password" name="password" id="password" />
            <label for="repeat_password">Repeat password:</label>
            {if !empty($error->repeat_password)}<p>{$error->repeat_password}</p>{/if}
            <input type="password" name="repeat_password" id="repeat_password" />
            <label for="email">Mail:</label>
            {if !empty($error->email)}<p>{$error->email}</p>{/if}
            <input type="email" name="email" id="email" />
            <label for="date">Birth date:</label>
            {if !empty($error->date)}<p>{$error->date}</p>{/if}
            <input type="date" name="date" id="date" min="1998-01-01" />
            <input type="submit" value="Sign in" />
        </form>
        {/if}
        </div>
    </div>
</main>