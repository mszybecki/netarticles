<main class="user">
    <div class="container">
        <div>
        {if $signout}
            <p>Thanks for using our website</p>
        {else}
            {if $try}
                <p>Something went wrong with deleting your account</p>
            {else}
                <form method="post" action="{$path}signout">
                    <label>Do you really want delete you account?</label>
                    <input type="hidden" name="confirm" value="true" />
                    <input type="submit" value="Yes, I want delete my accout" />
                </form>
            {/if}
        {/if}
        </div>
    </div>
</main>