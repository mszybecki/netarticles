{if $empty}
	<div class="holder">
		{foreach from=$categories item=$category}
		    <p><a href='{$path}panel/attendant/{$category['name']}'>{$category['name']}</a></p>
		{/foreach}
	</div>
{else}
	{assign var="curentCategory" value=$category}
	<div class="add">
	    <form action="{$path}panel/attendant/{$curentCategory}" method="post">
	        <input type="hidden" name="action" value="add" />
	        <select name="user">
				{foreach from=$users item=$user}
					<option value="{$user['id']}">{$user['nick']}</option>
				{/foreach}
	        </select>
	        <input type="submit" value="+" />
	    </form>
	</div>

	<div class="holder">
		{foreach from=$attendants item=$attendant}
			<form method="post" action="{$path}panel/attendant/{$curentCategory}">
				<input type="hidden" name="action" value="delete">
				<input type="hidden" name="user" value="{$attendant['id']}">
				<input type="text" value="{$attendant['nick']}">
				<button type="submit" class="ico-delete"></button>
			</form>
		{/foreach}
	</div>
{/if}