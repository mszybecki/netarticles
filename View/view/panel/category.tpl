{include 'add.tpl'}
{include 'result.tpl'}

<div class="holder">
    {foreach from=$categories item=$category}
    <form action="{$path}panel/category" method="post">
        <input type="text" name="newName" value="{$category['name']}" />
        <input type="hidden" name="oldName" value="{$category['name']}" />
        <input type="hidden" name="action" value="edit" />
        <button type="submit" class="ico-edit"></button>
        <button type="submit" form="remove{$category['id']}" class="ico-delete"></button>
    </form>
    <form action="{$path}panel/category" method="post" id="remove{$category['id']}">
        <input type="hidden" name="name" value="{$category['name']}" />
        <input type="hidden" name="action" value="remove" />
    </form>
    {/foreach}
</div>
