{if $empty}
    <div class="select">select category</div><div class="holder">
    {foreach from=$categories item=$category}
        <p><a href='{$path}panel/param/{$category['name']}'>{$category['name']}</a></p>
    {/foreach}
{else}
    {include "add.tpl"}
    {include "result.tpl"}
<div class="holder">
    {foreach from=$params item=$param}
    <form action="{$path}panel/param/{$curentCategory}" method="post">
        <select name="category" default="{$curentCategory}">
            {foreach from=$categories item=$category}
                <option {if $category['name'] eq $curentCategory}selected{/if} value="{$category['name']}">{$category['name']}</option>
            {/foreach}
        </select>
        <input type="text" name="newName" value="{$param['name']}" />
        <input type="hidden" name="oldName" value="{$param['name']}" />
        <input type="hidden" name="action" value="edit" />
        <button type="submit" class="ico-edit"></button>
        <button type="submit" form="remove{$param['id']}" class="ico-delete"></button>
    </form>
    <form action="{$path}panel/param/{$curentCategory}" method="post" id="remove{$param['id']}">
        <input type="hidden" name="name" value="{$param['name']}" />
        <input type="hidden" name="action" value="remove" />
    </form>
    {/foreach}
</div>
{/if}
