<div class="holder">
    <form method="post" action="{$path}panel/product/{$product['id']}">
        <input type="hidden" name="update" value="true">
        <!-- name + prize -->
        <input type="text" name="name" value="{$product['name']}" />
        <input type="text" name="prize" value="{$product['prize']}" />
        <br /><br />

        <!-- category of product -->
        <input disabled type="text" value="Category" />
        <select name="category">
        {foreach from=$categories item=$category}
            <option value="{$category['id']}" {if $category['id'] == $product['id_category']}selected{/if}>{$category['name']}</option>
        {/foreach}
        </select>
        <br /><br />
        <input type="text" value="Sale" />
        <input type="text" name="sale" value="{$product['sale']}">
        <br /><br />

        <!-- params of product -->
        {for $i=0 to count($product['params']) - 1}
            <select name="param[{$i}][param]">
                {foreach from=$params item=$param}
                    <option value="{$param['id']}" {if $param['id'] == $product['params'][$i]['id']}selected{/if}>{$param['name']}</option>
                {/foreach}
            </select>
            <input type="text" name="param[{$i}][value]" value="{$product['params'][$i]['value']}" style="margin-bottom: 20px;" />
            <br />
        {/for}

        <!-- add produc -->
        <select name="param[{count($product['params'])}][param]">
            {foreach from=$params item=$param}
                <option value="{$param['id']}">{$param['name']}</option>
            {/foreach}
        </select>
        <input type="text" name="param[{count($product['params'])}][value]" />
        <button type="submit" class="ico-edit"></button>
    </form>
    <form method="post" action="{$path}panel/product/{$product['id']}" enctype="multipart/form-data">
        <input type="hidden" name="img" value="1">
        <input type="file" name="pFile">
        <input type="submit" value="Upload file">
    </form>
    <img alt="" src="{$path}img/{$product['img']}" />
</div>