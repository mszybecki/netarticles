{include 'add.tpl'}

<div class="holder">
    {foreach from=$products item=$product}
    <form method="post" action="{$path}panel/product">
        <input type="hidden" value="{$product['id']}" />
        <input type="text" value="{$product['name']}" />
        <input type="text" value="{$product['prize']}" />
        <button type="submit" class="ico-delete" form="delete{$product['id']}"></button>
        <button type="submit" class="ico-edit" form="details{$product['id']}"></button>
    </form>
    <form method="post" action="{$path}panel/product" id="delete{$product['id']}">
        <input type="hidden" name="action" value="delete" />
        <input type="hidden" name="id" value="{$product['id']}" />
    </form>
    <form method="post" action="{$path}panel/product/{$product['id']}" id="details{$product['id']}"></form>
    {/foreach}
</div>
