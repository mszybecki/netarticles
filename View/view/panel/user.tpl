<div class="holder">
    {foreach from=$users item=$user}
    <form method="post" action="{$path}panel/user">
        <input type="hidden" value="{$user['nick']}"  name="oldName" />
        <input type="text" value="{$user['nick']}"  name="newName" />
        <input type="text" value="{$user['email']}" name="email"/>
        <input type="text" value="{$user['birth']}" disabled />
        <select name="type">
            <option value="normal" {if $user['type'] eq "normal"}selected{/if}>normal</option>
            <option value="supervisior" {if $user['type'] eq "supervisior"}selected{/if}>supervisior</option>
            <option value="admin" {if $user['type'] eq "admin"}selected{/if}>admin</option>
        </select>
        <button type="submit" class="ico-edit"></button>
        <button type="submit" class="ico-delete" form="remove{$user['id']}"></button>
    </form>
    <form method="post" action="{$path}panel/user" id="remove{$user['id']}">
		<input type="hidden" name="action" value="remove">
		<input type="hidden" name="id" value="{$user['id']}">
	</form>
    {/foreach}
</div>