<main class="home">
	<div class="baner">
		<div class="container">
			<div>
				<p>{$baner['name']}</p>
				<p>{$baner['description']}</p>
				<a class="button" href="{$path}product/{$baner['id']}">More</a>
			</div>
			<div>
				<img alt="" src="{$path}img/{$baner['img']}">
			</div>
		</div>
	</div>
	<div class="sales">
		<div class="container">
			<div class="products">
				<span>Sales</span>
				<div class="row">
                    {foreach from=$sales item=$sale}
					<div>
						<a href="{$path}product/{$sale['id']}">
							<img src="{$path}img/{$sale['img']}" alt="">
							<div class="stars">
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
							</div>
							<p>{$sale['name']}</p>
						</a>
						<a href="{$path}cart/add?id={$sale['id']}">
                        {if empty($sale['sale'])}
                            <span>{$sale['prize']} zł</span>
                        {else}
                            <span>{$sale['sale']} zł<span>{$sale['prize']} zł</span></span>
                        {/if}
                        </a>
					</div>
                    {/foreach}
                </div>
			</div>
		</div>
	</div>
	<div class="newest">
		<div class="container">
			<div class="products">
				<span>Newest</span>
				<div class="row">
                    {foreach from=$newestes item=$newest}
					<div>
						<a href="{$path}product/{$newest['id']}">
							<img src="{$path}img/{$newest['img']}" alt="">
							<div class="stars">
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
								<div class="star"></div>
							</div>
							<p>{$newest['name']}</p>
						</a>
						<a href="{$path}cart/add?id={$sale['id']}"><span>{$newest['prize']} zł</span></a>
					</div>
                    {/foreach}
				</div>
			</div>
		</div>
	</div>
</main>
