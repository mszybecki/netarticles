<main class="home cart">
{if $cart['length'] == 0}
    <div class="container">
        <p>Your cart is empty</p>
    </div>
{else}
    <form method="post" action="{$path}cart">
    {foreach from=$cart['products'] item=$product}
        <div class="container">
            <div class="row">
                <div>
                    <img alt="" src="{$path}img/{$product['object']['img']}" />
                    <p>{$product['object']['name']}</p>
                </div>
                <div>
                    <p>Number of product/-s</p>
                    <input type="text" name="product[{$product['object']['id']}]" value="{$product['number']}" />
                </div>
                <div>
                    <p>Parameters</p>
                    {foreach from=$product['object']['params'] item=$param}
                        <p>{$param['name']}: {$param['value']}</p>
                    {/foreach}
                </div>
            </div>
        </div>
        {/foreach}
        <input type="submit" value="Save cart" />
    </form>
{/if}
</main>