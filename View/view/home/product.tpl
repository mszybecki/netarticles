<main class="home product">
    <div class="container">
    {if $exist}
        <div class="row">
            <div>
                <p>{$product['name']}</p>
                <p>{$product['description']}</p>
            </div>
            <div>
                <img alt="" src="{$path}img/{$product['img']}" />
            </div>
        </div>
        {foreach from=$product['params'] item=$param}
            <div class="row">
                <p>{$param['name']}</p>
                <p>{$param['value']}</p>
            </div>
        {/foreach}
        <a href="{$path}cart/add?id={$product['id']}">
        {if empty($product['sale'])}
            <span>{$product['prize']} zł</span>
        {else}
            <span>{$product['sale']} zł<span>{$product['prize']} zł</span></span>
        {/if}
        </a>
    {/if}
    </div>
</main>