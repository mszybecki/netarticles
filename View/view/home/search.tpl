{if isset($search) && $search === false}
    <p>Select category</p>
{else}
    {if isset($category) && $category === false}
        <p>Invalid category</p>
    {else}
        {if empty($products)}
            <p>No products in this category</p>
        {else}
            <div class="products">
                <div class="container">
                    <div class="row">
                    {foreach from=$products item=$product}
                        <div>
					        <a href="{$path}product/{$product['id']}">
						        <img src="{$path}img/{$product['img']}" alt="">
						        <div class="stars">
							        <div class="star"></div>
							        <div class="star"></div>
							        <div class="star"></div>
							        <div class="star"></div>
							        <div class="star"></div>
						        </div>
						        <p>{$product['name']}</p>
					        </a>
					        <a href="{$path}cart/add?id={$product['id']}">
                            {if empty($product['sale'])}
                                <span>{$product['prize']} zł</span>
                            {else}
                                <span>{$product['sale']} zł<span>{$product['prize']} zł</span></span>
                            {/if}
                            </a>
				        </div>
                    {/foreach}
                    </div>
                </div>
            </div>
        {/if}
    {/if}
{/if}