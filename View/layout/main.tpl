﻿<!DOCTYPE HTML>
<html>

	<head>

		<title>{$title}</title>

		<meta charset="utf-8" />
		<meta name="robots" content="noindex,nofollow">

		<link rel="icon" href="{$path}favicon.ico" />

		<link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{$path}css/style.css" />

	</head>

	<body>

         <header>
			<div class="container">
				<div class="subheader">
					<h1><a href="{$path}">NetArticles</a></h1>
					<form method="post" action="#">
						<input type="text" name="query">
						<button type="submit"></button>
					</form>
					<div class="user">
						{if isset($nick)}<a href="{$path}user">{$nick}</a>{else}<a href="{$path}login">Log in</a>{/if}
                        {if isset($nick)}<a href="{$path}logout">Log out</a>{else}<a href="{$path}signin">Sign in</a>{/if}
					</div>
					<div class="cart">
						<a href="{$path}cart">{$cart['length']}</a>
					</div>
				</div>
				<nav>
					<ul>
		            {foreach from=$categories item=$category}
                        <li><a href="{$path}search/{$category['name']}">{$category['name']}</a></li>
                    {/foreach}
					</ul>
				</nav>
			</div>
		</header>

        {include $content}

        <footer>
			<div class="subfooter">
				<div class="container">
					<div>
						<span>O nas</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
					<div>
						<div>
							<p>Your account</p>
							<ul>
								<li><a href="{$path}cart">Cart</a></li>
						        <li>{if isset($nick)}<a href="{$path}panel">Panel</a>{else}<a href="{$path}login">Log in</a>{/if}</li>
                                <li>{if isset($nick)}<a href="{$path}logout">Log out</a>{else}<a href="{$path}signin">Sign in</a>{/if}</li>
								<li><a href="{$path}">Your order</a></li>
							</ul>
						</div>
						<div>
							<p>Sales</p>
							<ul>
		                    {foreach from=$categories item=$category}
                                <li><a href="{$path}search/{$category['name']}">{$category['name']}</a></li>
                            {/foreach}
							</ul>
						</div>
						<div>
							<p>Information</p>
							<ul>
								<li>Delivery</li>
								<li>Instalments</li>
								<li>Product return</li>
								<li>Warranty</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="subfooter">
				<div class="container">
					<div>
						<p>+48 503 298 222</p>
						<p>Mon-Sat 7-19</p>
						<p>Sun 10-14</p>
					</div>
					<div>
						<p>Check out us on</p>
						<ul>
							<li></li>
							<li></li>
							<li></li>
							<li></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>

	</body>

</html>
