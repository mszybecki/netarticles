<!DOCTYPE HTML>
<html>

    <head>
        
        <title>NetArticles' panel</title>
        <meta charset="utf-8">
        <meta name="robots" content="noindex,nofollow">
    
        <link rel="icon" href="{$path}favicon.ico" />

        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{$path}css/panel.css">
        
    </head>

    <body>

        <header>
            <div>
                <h1>NetArticles</h1>
                <h2>panel</h2>
            </div>

            <nav>
                {foreach from=$navs item=nav}
                <div {if $action eq $nav}class="active"{/if}><a href="{$path}panel/{$nav}">{$nav}</a></div>
                {/foreach}
            </nav>
        </header>

        <main>

            {include $content}

        </main>

        <script src="{$path}js/jquery.min.js"></script>
        <script src="{$path}js/panel.js"></script>

    </body>

</html>