<?php

namespace Model;

class User
{
    public $connection;

    public function add(string $nick, string $password, string $email, string $birth)
    {
        $stmt = $this->connection->prepare('INSERT INTO user (`nick`, `password`, `email`, `birth`, `type`) VALUES (:nick, :password, :email, :birth, "normal")');
        $stmt->execute(array(":nick" => $nick, ":password" => $password, ":email" => $email, ":birth" => $birth));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete(string $nick)
    {
        $stmt = $this->connection->prepare('DELETE FROM user WHERE `nick`=:nick');
        $stmt->execute(array(":nick" => $nick));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editType(string $nick, string $type)
    {
        $stmt = $this->connection->prepare('UPDATE user SET `type`=:type WHERE `nick`=:nick');
        $stmt->execute(array(":type" => $type, ":nick" => $nick));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editPassword(string $nick, string $password)
    {
        $stmt = $this->connection->prepare('UPDATE user SET `password`=:password WHERE `nick`=:nick');
        $stmt->execute(array(":password" => $password, ":nick" => $nick));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editEmail(string $nick, string $email)
    {
        $id = $this->getId($nick);
        if ($id == 0) {
            return false;
        } else {
            $stmt = $this->connection->prepare('update user set email=:email where id=:id');
            $stmt->execute(array(':id' => $id, ':email' => $email));
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function editNick(string $oldName, string $newName)
    {
        $id = $this->getId($oldName);
        if ($id == 0) {
            return false;
        } else {
            $stmt = $this->connection->prepare('update user set nick=:newName where id=:id');
            $stmt->execute(array(':id' => $id, ':newName' => $newName));
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getId(string $nick)
    {
        $stmt = $this->connection->prepare('select id from user where `nick`=:nick');
        $stmt->execute(array(":nick" => $nick));
        $id = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$id) {
            return 0;
        } else {
            return $id['id'];
        }
    }

    public function getType(string $nick)
    {
        $stmt = $this->connection->prepare('select type from user where nick=:nick');
        $stmt->execute(array(":nick" => $nick));
        $type = $stmt->fetch(\PDO::FETCH_ASSOC)['type'];
        return $type;
    }

    public function getAll(string $type = null)
    {
        if (empty($type)) {
            $stmt = $this->connection->prepare("select id, nick, email, birth, type from user");
            $stmt->execute();
        } else {
            $stmt = $this->connection->prepare("select id, nick, email, birth from user where type = :type");
            $stmt->execute(array(":type" => $type));
        }
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function checkUser(string $nick, string $password)
    {
        $stmt = $this->connection->prepare("select id from user where nick=:nick and password=:password");
        $stmt->execute(array(":nick" => $nick, ":password" => $password));
        $id = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (empty($id)) {
            return false;
        } else {
            return true;
        }
    }

    public function checkEmail(string $email)
    {
        $stmt = $this->connection->prepare('select id from user where email=:email');
        $stmt->execute(array(":email" => $email));
        $id = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (empty($id)) {
            return false;
        } else {
            return true;
        }
    }

    public function addAttendant(int $category, int $user)
    {
        $stmt = $this->connection->prepare('INSERT INTO category_attendant (`id_category`, `id_user`) VALUES (:category, :user)');
        $stmt->execute(array(":user" => $user, ":category" => $category));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAttendants(int $category)
    {
        $stmt = $this->connection->prepare("select user.id, user.nick, user.email from user inner join category_attendant on category_attendant.id_user = user.id where category_attendant.id_category = :category");
        $stmt->execute(array(":category" => $category));
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function getCategoriesOfAttendant(int $user)
    {
        $stmt = $this->connection->prepare("select category.id, category.name from category inner join category_attendant on category_attendant.id_category = category.id where category_attendant.id_user = :user");
        $stmt->execute(array(":user" => $user));
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function removeAttendant(int $category, int $user)
    {
        $stmt = $this->connection->prepare('delete from category_attendant where id_category = :category and id_user = :user');
        $stmt->execute(array(":category" => $category, ":user" => $user));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
