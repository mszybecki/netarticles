<?php

namespace Model;

class Product
{
    public $connection;

    public function add(string $name)
    {
        $stmt = $this->connection->prepare('insert into product (`name`) values (:name)');
        $stmt->execute(array(":name" => $name));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function delete(int $id)
    {
        $stmt = $this->connection->prepare('delete from product where id = :id');
        $stmt->execute(array(":id" => $id));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll(int $category = null)
    {
        if (!empty($category)) {
            $stmt = $this->connection->prepare('select id from product where `id_category`=:category');
            $stmt->execute(array(":category" => $category));
        } else {
            $stmt = $this->connection->prepare('select id from product order by id');
            $stmt->execute();
        }

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        for ($i = 0; $i < count($result); $i++) {
            $res[] = $this->get($result[$i]['id']);
        }

        return $res;
    }

    public function get(int $id)
    {
        $stmt = $this->connection->prepare('select * from product where `id`=:id');
        $stmt->execute(array(":id" => $id));
        $res = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (empty($res)) {
            return NULL;
        }

        $stmt = $this->connection->prepare('select param.id, param.name, product_params.value from product_params inner join param on param.id = product_params.id_param where product_params.id_product=:id order by param.name');
        $stmt->execute(array(":id" => $res['id']));
        $res['params'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $stmt = $this->connection->prepare('select value from sale where id_product=:id');
        $stmt->execute(array(":id" => $res['id']));
        $res['sale'] = $stmt->fetch(\PDO::FETCH_NUM)[0] ?? null;

        return $res;
    }

    public function getThree(string $what)
    {
        if ($what == 'newest') {
            $stmt = $this->connection->prepare("select * from product order by date desc limit 3");
            $stmt->execute();
            $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            for ( $i = 0; $i < count($res); $i++ )
            {
                $stmt = $this->connection->prepare('select param.name, product_params.value from product_params inner join param on param.id = product_params.id_param where product_params.id_product=:id');
                $stmt->execute(array(":id" => $res[$i]['id']));
                $res[$i]['params'] = $stmt->fetchAll(\PDO::FETCH_ASSOC);

                $stmt = $this->connection->prepare('select value from sale where id_product=:id');
                $stmt->execute(array(":id" => $res[$i]['id']));
                $res[$i]['sale'] = $stmt->fetchAll(\PDO::FETCH_ASSOC)[0] ?? null;
            }
            return $res;
        } else if ($what == 'sale') {
            $stmt = $this->connection->prepare("select id_product from sale order by date desc limit 3");
            $stmt->execute();
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            for ($i = 0; $i < count($result); $i++) {
                $res[] = $this->get($result[$i]['id_product']);
            }
            return $res;
        }
        return null;
    }

    public function getRandom()
    {
        $stmt = $this->connection->prepare('select id from product order by rand() limit 1');
        $stmt->execute();
        return $this->get($stmt->fetch(\PDO::FETCH_ASSOC)['id']);
    }

    public function editCategory(int $id, int $category)
    {
        $stmt = $this->connection->prepare('update product set id_category=:category where id=:id');
        $stmt->execute(array(":category" => $category, ":id" => $id));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editName(int $id, string $name)
    {
        $stmt = $this->connection->prepare('update product set name=:name where id=:id');
        $stmt->execute(array(":name" => $name, ":id" => $id));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editPrize(int $id, string $prize)
    {
        $stmt = $this->connection->prepare('update product set prize=:prize where id=:id');
        $stmt->execute(array(":prize" => $prize, ":id" => $id));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addParam(int $id_product, int $id_param, string $value)
    {
        $stmt = $this->connection->prepare("insert into `product_params` (`id_product`, `id_param`, `value`) values (:id_product, :id_param, :value)");
        $stmt->execute(array(":id_product" => $id_product, ":id_param" => $id_param, ":value" => $value));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteParams(int $id_product)
    {
        $stmt = $this->connection->prepare("delete from product_params where id_product=:id_product");
        $stmt->execute(array(":id_product" => $id_product));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function addSale(int $id_product, float $sale)
    {
        $stmt = $this->connection->prepare('insert into sale (id_product, value) values (:id_product, :sale)');
        $stmt->execute(array(':id_product' => $id_product, ':sale' => $sale));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getSale(int $id_product)
    {
        $stmt = $this->connection->prepare('select value from sale where id_product = :id_product');
        $stmt->execute(array(':id_product' => $id_product));
        return $stmt->fetch(\PDO::FETCH_ASSOC)['value'];
    }

    public function updateSale(int $id_product, float $sale)
    {
        $stmt = $this->connection->prepare('update sale set value = :value where id_product = :id_product');
        $stmt->execute(array(':id_product' => $id_product, ':value' => $sale));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function removeSale(int $id_product)
    {
        $stmt = $this->connection->prepare('delete from sale where id_product = :id_product');
        $stmt->execute(array(':id_product' => $id_product));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateImg(int $id, string $name)
    {
        $stmt = $this->connection->prepare('update product set img = :img where id = :id');
        $stmt->execute(array(':id' => $id, ':img' => $name));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
