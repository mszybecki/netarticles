<?php

namespace Model;

class Category
{
    public $connection;

    public function add(string $name)
    {
        $stmt = $this->connection->prepare('insert into category (`name`) values (:name)');
        $stmt->execute(array(':name' => $name));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function remove(string $name)
    {
        $stmt = $this->connection->prepare('delete from category where name=:name');
        $stmt->execute(array(':name' => $name));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function rename(string $oldName, string $newName)
    {
        $stmt = $this->connection->prepare('update category set name=:newname where name=:oldname');
        $stmt->execute(array(':oldname' => $oldName, ':newname' => $newName));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getId(string $name)
    {
        $stmt = $this->connection->prepare('select id from category where `name`=:name');
        $stmt->execute(array(":name" => $name));
        $id = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!$id) {
            return 0;
        } else {
            return $id['id'];
        }
    }

    public function getName(int $id)
    {
        $stmt = $this->connection->prepare('select name from category where `id`=:id');
        $stmt->execute(array(":id" => $id));
        $name = $stmt->fetch(\PDO::FETCH_ASSOC)['name'];
        if (!$name) {
            return 0;
        } else {
            return $name;
        }
    }

    public function getAll(int $mode = 0)
    {
        if ($mode == 0) {
            $stmt = $this->connection->prepare('select id, name from category where active = 1');
        } else {
            $stmt = $this->connection->prepare('select id, name from category');
        }
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function addAttendant(int $user, string $category)
    {
        $category = $this->getId($category);
        if ($user < 1 || $category == 0) {
            return false;
        } else {
            $stmt = $this->connection->prepare('insert into category_attendant (`id_category`, `id_user`) values (:category, :user)');
            $stmt->execute(array(":category" => $category, ":user" => $user));
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function removeAttendant(int $id)
    {
        $stmt = $this->connection->prepare('delete from category_attendant where id=:id');
        $stmt->execute(array(":id" => $id));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editAttendant(int $id, int $user)
    {
        if ($id < 1 || $user < 1) {
            return false;
        } else {
            $stmt = $this->connection->prepare('update category_attendant set id_user=:user where id=:id');
            $stmt->execute(array(":id" => $id, ":user" => $user));
            if ($stmt->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getAllAttendants(int $mode = 0)
    {
        $stmt = $this->connection->prepare('select category.id as "id_category", user.id as "id_user" from category_attendant left join user on user.id = category_attendant.id_user right join category on category.id = category_attendant.id_category');
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
