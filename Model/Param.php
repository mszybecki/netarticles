<?php

namespace Model;

class Param
{
    public $connection;

    public function getAll(string $category)
    {
        $stmt = $this->connection->prepare('select param.id, param.name from param inner join category on category.id = param.id_category where category.name = :category');
        $stmt->execute(array(":category" => $category));
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function add(string $category, string $name)
    {
        $stmt = $this->connection->prepare('insert into param (`id_category`, `name`) values ((select id from category where name=:category), :name)');
        $stmt->execute(array(":category" => $category, ":name" => $name));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function remove(string $category, string $name)
    {
        $stmt = $this->connection->prepare("delete from param where `id_category`=(select id from category where name=:category) and `name`=:name");
        $stmt->execute(array(":name" => $name, ":category" => $category));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edit(string $oldCategory, string $newCategory, string $oldName, string $newName)
    {
        $stmt = $this->connection->prepare("update param set `id_category`=(select id from category where name=:newCategory), `name`=:newName where `id_category`=(select id from category where name=:oldCategory) and `name`=:oldName");
        $stmt->execute(array(":newCategory" => $newCategory, ":oldCategory" => $oldCategory, ":oldName" => $oldName, ":newName" => $newName));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function rename(string $category, string $oldName, string $newName)
    {
        $stmt = $this->connection->prepare("update param set name=:newName where `id_category`=(select id from category where name=:category) and `name`=:oldName");
        $stmt->execute(array(":oldName" => $oldName, ":newName" => $newName, ":category" => $category));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function editCategory(string $name, string $category)
    {
        $stmt = $this->connection->prepare("update param set id_category=(select id from category where name=:category) where name=:name");
        $stmt->execute(array(":category" => $category, ":name" => $name));
        if ($stmt->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
