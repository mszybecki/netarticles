<?php

namespace Module\Resource\Controller;

class ResourceController extends \Core\Controller\ActionController
{
    public function __construct()
    {

    }

    public function beforeRun($data, $action)
    {
        if ($action != "ico") {
            $this->file = "../public/$action/$data";
        } else {
            $this->file = "../public/img/$data";
        }
        if (!file_exists($this->file)) {
            exit();
        }
        $this->viewModel->render = false;
        $this->dontCacheLast();
        if ($action == "css") {
            header('Content-Type: text/css;charset=UTF-8');
        } else if ( $action == "js") {
            header('Content-Type: application/javascript;charset=UTF-8');
        } else {
            header('Content-Type: ' . mime_content_type($this->file));
        }
        $this->showFile();
    }

    public function fontsAction(){}
    public function cssAction(){}
    public function icoAction(){}
    public function imgAction(){}
    public function jsAction(){}

    private function showFile()
    {
        header('Accept-Ranges: bytes');
        header('Connection: Keep-Alive');
        header('Content-Length: ' . filesize($this->file));
        header('Last-Modified: ' . filemtime($this->file));
        header('Keep-Alive: timeout=2, max=1000');
        header('Cache-Control: public, max-age=31536000');
        header('Pragma: cache');
        readfile($this->file);
    }
}