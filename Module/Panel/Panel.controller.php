<?php

namespace Module\Panel\Controller;

use \Core\Utility;

class PanelController extends \Core\Controller\ActionController
{
    public function __construt()
    {
        $this->supervisiorActions = array("param", "product");
    }

    public function beforeRun($data, $action)
    {
        $this->viewModel->layout = "panel";

        $this->model->load("user");
        $this->model->load("category");

        if (!$this->session->check("logged", true)) {
            $this->dontCacheLast();
            Utility::redirect("error");
        } else {
            if ($this->user->getType($this->session->nick) == "normal") {
                $this->dontCacheLast();
                $this->model->unload();
                Utility::redirect("error");
            }
        }


        if ($this->user->getType($this->session->nick) == "admin") {
            $this->viewModel->navs = array("category", "param", "user", "product", "attendant");
            $this->viewModel->categories = $this->category->getAll(1);
        } else if ($this->user->getType($this->session->nick) == "supervisior") {
            $this->viewModel->navs = array("param", "product");
            $this->viewModel->categories = $this->user->getCategoriesOfAttendant($this->user->getId($this->session->nick));
            $this->viewModel->categories = array_merge($this->viewModel->categories, array(array('id' => '21', 'name' => 'uncategorized')));
        } else {
            $this->dontCacheLast();
            Utility::redirect("error");
        }
    }

    public function indexAction()
    {
    }

    public function categoryAction()
    {
        $this->model->load("category");
        $this->viewModel->categories = $this->category->getAll(0);

        if ($this->session->check("result")) {
            $this->viewModel->result = $this->session->result;
            $this->session->remove("result");
        }

        if (!isset($this->post->action)) {
            $this->viewModel->what = "all";
            $this->model->unload();
            return;
        } else if ($this->post->action == "add") {
            if (isset($this->post->name) && !empty($this->post->name)) {
                $this->session->result = $this->category->add($this->post->name);
                $this->session->save();
            } else {
                $this->session->result = false;
                $this->session->save();
            }
        } else if ($this->post->action == "edit") {
            if (isset( $this->post->newName ) && isset( $this->post->oldName )) {
                $this->session->result = $this->category->rename($this->post->oldName, $this->post->newName);
                $this->session->save();
            }
        } else if ($this->post->action == "remove") {
            if (isset( $this->post->name)) {
                $this->session->result = $this->category->remove($this->post->name);
                $this->session->save();
            }
        }

        $this->model->unload();
        Utility::redirect("panel/category");
    }

    public function paramAction($category)
    {

        $this->model->load("category");

        if (empty($category)) {
            $this->viewModel->empty = true;
        } else {
            if ($this->category->getId($category) != 0) {
                if ($this->post->request) {
                    $this->model->load("param");

                    if ($this->post->action == "add") {
                        $this->viewModel->result = $this->param->add($category, $this->post->name);
                    } else if ($this->post->action == "edit") {
                        $this->viewModel->result = $this->param->edit($category, $this->post->category, $this->post->oldName, $this->post->newName);
                    } else if ($this->post->action == "remove") {
                        $this->viewModel->result = $this->param->remove($category, $this->post->name);
                    }
                }

                $this->model->load("param");

                $this->viewModel->curentCategory = $category;
                $this->viewModel->empty    = false;
                $this->viewModel->params = $this->param->getAll($category);
            } else {
                $this->dontCacheLast();
                Utility::redirect("panel/param");
            }
        }
        $this->model->unload();
    }

    public function userAction()
    {
        $this->model->load("user");

        if ($this->post->request) {
            $this->model->load("user");
			
			if (isset($this->post->action) && $this->post->action == "remove") {
				$this->user->delete($this->post->id);
			}
				
            if (isset($this->post->newName) && isset($this->post->oldName)) {
                $this->viewModel->result['name'] = $this->user->editNick($this->post->oldName, $this->post->newName);
            } 
            if (isset($this->post->email) && isset($this->post->oldName)) {
                $this->viewModel->result['email'] = $this->user->editEmail($this->post->oldName, $this->post->email);
            }
            if (isset($this->post->type) && isset($this->post->oldName)) {
                $this->viewModel->result['type'] = $this->user->editType($this->post->oldName, $this->post->type);
            }
        }

        $this->viewModel->users = $this->user->getAll();

        $this->model->unload();
    }

    public function productAction($data)
    {
        $this->model->load("product");
        $this->model->load("param");
        $this->model->load("category");
        $this->model->load("user");

        if (empty($data)) {
            if ($this->post->request) {
                if (!empty($this->post->action)) {
                    if ($this->post->action == "add") {
                        $this->product->add($this->post->name);
                    }
                    if ($this->post->action == "delete") {
                        $this->product->deleteParams($this->post->id);
                        $this->product->removeSale($this->post->id);
                        $this->product->delete($this->post->id);
                        @unlink("img/".$this->post->id.".png");
                    }
                }
            }

            if ($this->user->getType($this->session->nick) == "supervisior") {
                $tmp = array();
                foreach ($this->viewModel->categories as $key => $category) {
                    if (($tmp_products = $this->product->getAll($this->category->getId($category['name']))) !== null) {
                        $tmp = array_merge($tmp_products, $tmp);
                    }
                }

                $this->viewModel->products = $tmp;
            } else {
                $this->viewModel->products = $this->product->getAll();
            }
        } else {
            if ($this->post->request) {
                if (!isset($this->post->img)) {
                    if (isset($this->post->update) && $this->post->update) {
                        if (!empty($this->post->category)) {
                            $this->product->editCategory($data, $this->post->category);
                        }
                        if (!empty($this->post->name)) {
                            $this->product->editName($data, $this->post->name);
                        }
                        if (!empty($this->post->prize)) {
                            $this->product->editPrize($data, $this->post->prize);
                        }
                        if (!empty($this->post->param)) {
                            $this->product->deleteParams($data);
                            for ($i = 0; $i < count($this->post->param); $i++) {
                                if (!empty($this->post->param[$i]['value'])) {
                                    $this->product->addParam($data, $this->post->param[$i]['param'], $this->post->param[$i]['value']);
                                }
                            }
                        }
                        if (($dSale = $this->product->getSale($data)) != $this->post->sale) {
                            if (empty($dSale)) {
                                if (!empty($this->post->sale)) {
                                    $this->product->addSale($data, floatval($this->post->sale));
                                }
                            } else {
                                if (empty($this->post->sale)) {
                                    $this->product->removeSale($data);
                                } else {
                                    $this->product->updateSale($data, floatval($this->post->sale));
                                }
                            }
                        }
                    }
                } else {
                    if (!empty($_FILES['pFile']['size'])) {
                        if (($check = getimagesize($_FILES["pFile"]["tmp_name"])) !== false) {
                            if ($check['mime'] == "image/png" && $_FILES["pFile"]["size"] < 500000) {
                                if (file_exists('img/$data.png')) {
                                    unlink("img/$data.png");
                                }
                                move_uploaded_file($_FILES["pFile"]["tmp_name"], "img/$data.png");
                                $this->product->updateImg($data, "$data.png");
                            }
                        }
                    }
                }
            }

            $this->viewModel->view    = "productDetails";
            $this->viewModel->product = $this->product->get($data);
            $this->viewModel->params  = $this->param->getAll($this->category->getName($this->viewModel->product['id_category']));
        }

        $this->model->unload();
    }

    public function attendantAction($category)
    {
        $this->viewModel->category = $category;

        if (empty($category)) {
            $this->viewModel->empty = true;
        } else {
            $this->model->load('category');
            $this->model->load('user');

            if (empty($this->category->getId($category))) {
                $this->model->unload();
                Utility::redirect('panel/attendant');
            }

            if ($this->post->request) {
                if (isset($this->post->action)) {
                    if ($this->post->action == "add") {
                        $this->user->addAttendant($this->category->getId($category), $this->post->user);
                    } else if ($this->post->action == "delete") {
                        $this->user->removeAttendant($this->category->getId($category), $this->post->user);   
                    }
                }
            }

            $this->viewModel->attendants = $this->user->getAttendants($this->category->getId($category));
            $this->viewModel->users = $this->user->getAll('supervisior');
            $this->viewModel->empty = false;

            $this->model->unload();
        }
    }
}