<?php

return array(
    'modules' => array(
        'Home',
        'User',
        'Panel',
        'Error',
        'Resource'
        ),
    'routes' => array(
        '/netarticles(/)?' => array(
            'controller' => 'Home',
            'action' => 'index'
        ),
        /* home */
        '/netarticles/search(/[:data]){0,1}' => array(
            'controller' => 'Home',
            'action' => 'search',
            ':data' => '[a-zA-Z]{0,}'
        ),
        '/netarticles/cart(/[:data]){0,1}' => array(
            'controller' => 'Home',
            'action' => 'cart',
            ':data'  => '[a-zA-Z0-9&]{0,}'
        ),
        '/netarticles/product/[:data]' => array(
            'controller' => 'Home',
            'action' => 'product',
            ':data'  => '[0-9]{0,}'
        ),
        /* sources */
        '/netarticles/css/[:data]' => array(
            'controller' => 'Resource',
            'action' => 'css',
            ':data' => '[\w.-]{1,}'
        ),
        '/netarticles/fonts/[:data]' => array(
            'controller' => 'Resource',
            'action' => 'fonts',
            ':data' => '[\w.-]{1,}'
        ),
        '/netarticles/js/[:data]' => array(
            'controller' => 'Resource',
            'action' => 'js',
            ':data' => '[\w.-]{1,}'
        ),
        '/netarticles/favicon.ico' => array(
            'controller' => 'Resource',
            'action' => 'ico',
            'data' => "favicon.ico"
        ),
        '/netarticles/img/[:data]' => array(
            'controller' => 'Resource',
            'action' => 'img',
            ':data' => '[\w.-]{1,}'
        ),
        /* user */
        '/netarticles/login' => array(
            'controller' => 'User',
            'action' => 'login'
        ),
        '/netarticles/logout' => array(
            'controller' => 'User',
            'action' => 'logout'
        ),
        '/netarticles/signin' => array(
            'controller' => 'User',
            'action' => 'signin'
        ),
        '/netarticles/signout' => array(
            'controller' => 'User',
            'action' => 'signout'
        ),
        '/netarticles/user' => array(
            'controller' => 'User',
            'action' => 'panel'
        ),
        '/netarticles/change/[:data]' => array(
            'controller' => 'User',
            'action' => 'change',
            ':data' => '[a-z]{0,}'
        ),
        /* panel */
        '/netarticles/panel(/[:action](/[:data]){0,}){0,1}' => array(
            'controller' => 'Panel',
            'action' => 'index',
            ':action' => '[a-z]',
            ':data' => '[a-zA-Z0-9]{0,}'
        )
    )
);
