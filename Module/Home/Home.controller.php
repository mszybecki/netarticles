<?php

namespace Module\Home\Controller;

use \Core\Utility;

class HomeController extends \Core\Controller\ActionController
{
    public function __construct()
    {

    }

    public function indexAction()
    {
        $this->model->load('product');

        $this->viewModel->baner    = $this->product->getRandom();
        $this->viewModel->sales    = $this->product->getThree('sale');
        $this->viewModel->newestes = $this->product->getThree('newest');
        $this->viewModel->title    = "NetArticles";

        $this->model->unload();
    }

    public function searchAction($category)
    {
        if ( !empty($category) )
        {
            $this->viewModel->title = "Search | " . ucfirst($category);

            $this->model->load("category");
            $this->model->load("product");

            if (($id = $this->category->getId($category)) == 0) {
                $this->viewModel->category = false;
                $this->model->unload();
                return;
            }

            $this->viewModel->products = $this->product->getAll($id);

            $this->model->unload();
        } else {
            $this->viewModel->title = "Search";
            $this->viewModel->search = false;
        }
    }

    public function productAction($id)
    {
        if ($id == null) {
            Utility::redirect("search");
        } else {
            $this->model->load("product");

            $product = $this->product->get($id);

            if ($product === NULL) {
                $this->viewModel->title = "Doesn't exist";
                $this->viewModel->exist = false;
                return;
            }

            $this->viewModel->exist = true;
            $this->viewModel->title = $product['name'];
            $this->viewModel->product = $product;

            $this->model->unload();
        }
    }

    public function cartAction($action)
    {
        $this->model->load('product');

        if (!empty($action)) {
            if ($action == 'add') {
                if ($this->get->id > 0 && !empty($this->product->get($this->get->id))) {
                    if (isset($this->session->cart['products'][$this->get->id])) {
                        $this->session->cart['products'][$this->get->id] += 1;
                        $this->session->cart['length'] += 1;
                    } else {
                        $this->session->cart['products'][$this->get->id] = 1;
                        $this->session->cart['length'] += 1;
                    }
                }
            }
            $this->session->save();
            $this->model->unload();
            \Core\Utility::redirect('cart');
        }

        if ($this->post->request) {
            $length = 0;
            foreach ($this->post->product as $id => $number) {
                $length += $number;
                if ($number == 0) {
                    unset($this->session->cart['products'][$id]);
                } else {
                    $this->session->cart['products'][$id]    = $number;
                }
            }
            $this->session->cart['length'] = $length;
            $this->session->save();
            \Core\Utility::redirect('cart');
        }

        $tmp = $this->session->cart;
        if ($tmp['length'] != 0) {
            foreach ($tmp['products'] as $key => $number) {
                $this->viewModel->cart['products'][$key] = array();
                $this->viewModel->cart['products'][$key]['number'] = $number;
                $this->viewModel->cart['products'][$key]['object'] = $this->product->get((int) $key);
            }
        }
        $this->model->unload();
        $this->viewModel->title = 'Cart';
    }
}