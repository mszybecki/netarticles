<?php

namespace Module\Error\Controller;

class ErrorController extends \Core\Controller\ActionController
{
    public function __construct()
    {

    }

    public function indexAction()
    {
        $this->viewModel->title = "Error 404";
        $this->viewModel->layout = "clear";

        if (gettype($this->session->last) == "array") {
            $this->viewModel->preview = $this->session->last[0];
        } else {
            if (!!$this->session->last) {
                $this->viewModel->preview = $this->session->last;
            } else {
                $this->viewModel->preview = false;
            }
        }
    }
}