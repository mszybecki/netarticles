<?php

namespace Module\User\Controller;

use \Core\Utility;

class UserController extends \Core\Controller\ActionController
{
    public function __construct()
    {

    }

    public function indexAction()
    {
        if ($this->session->check("logged", true)) {
            $this->model->load("user");

            $this->viewModel->title = "Panel";
            $this->viewModel->nick  = $this->session->nick;
            $this->viewModel->type  = $this->user->getType($this->session->nick);
            $this->viewModel->view = "panel";

            $this->model->unload();
        } else {
            $this->viewModel->title = "Login";
            $this->viewModel->view = "login";
        }
    }

    public function loginAction()
    {
        if ($this->session->check("logged", true)) {
            Utility::redirect("user");
        }

        $this->viewModel->try = false;

        if ($this->post->request) {
            $nick = Utility::clearString($this->post->nick);
            $pass = Utility::clearString($this->post->password);

            $pass = Utility::hash($pass);

            $this->model->load("user");
            if ($this->user->checkUser($nick, $pass)) {
                $this->session->logged = true;
                $this->session->nick   = $nick;
                $this->session->save();

                Utility::redirect("user");
            } else {
                $this->viewModel->try = true;
            }
            $this->model->unload();
        }

        $this->viewModel->title = "Zaloguj się";
    }

    public function logoutAction()
    {
        $this->session->logged = false;
        $this->session->remove("nick");
        $this->session->save();
        unset($this->viewModel->nick);
        $this->viewModel->title = "Wylogowano";
    }

    public function panelAction()
    {
        if ($this->session->check("logged", true)) {
            $this->model->load("user");

            $this->viewModel->title = "Panel";
            $this->viewModel->type  = $this->user->getType($this->session->nick);

            $this->model->unload();
        } else {
            Utility::redirect("user");
        }
    }

    public function signinAction()
    {
        $this->viewModel->title = "Sign in";
        if ($this->post->request) {
            if (!isset($this->post->nick) || !isset($this->post->password) || !isset($this->post->repeat_password) || !isset($this->post->email) || !isset($this->post->date)) {
                return;
            }

            $this->viewModel->error = (object) array();
            $this->model->load("user");

            if (strlen($this->post->nick) < 5 || strlen($this->post->nick) > 32) {
                $this->viewModel->error->nick = "Length of nick has to be between 5 and 32 characters";
            }
            if ($this->user->getId($this->post->nick) != 0) {
                $this->viewModel->error->nick = "This nick already exists";
            }
            if (strlen($this->post->password) < 8 || strlen($this->post->password) > 64) {
                $this->viewModel->error->password = "Length of password has to be between 8 and 64 characters";
            }
            if ($this->post->password != $this->post->repeat_password) {
                $this->viewModel->error->repeat_password = "Passwords are not equal";
            }
            if (!filter_var($this->post->email, FILTER_VALIDATE_EMAIL)) {
                $this->viewModel->error->email = "Invalid email format";
            }
            if ($this->user->checkEmail($this->post->email)) {
                $this->viewModel->error->email = "This email already exists";
            }

            if (empty((array) $this->viewModel->error)) {
                $nick            = Utility::clearString($this->post->nick);
                $password        = Utility::clearString($this->post->password);
                $email           = Utility::clearString($this->post->email);
                $date            = Utility::clearString($this->post->date);

                if ($this->user->add($nick, Utility::hash($password), $email, $date)) {
                    $this->viewModel->signin = true;
                } else {
                    $this->viewModel->signin = false;
                }
            }

            $this->model->unload();
        }
    }

    public function signoutAction()
    {
        if ($this->session->check("logged", true)) {
            if ($this->post->request && $this->post->confirm == true) {
                $this->model->load("user");

                if ($this->user->delete($this->session->nick)) {
                    $this->session->logged = false;
                    $this->session->remove("nick");
                    $this->session->save();

                    $this->viewModel->signout = true;
                    $this->viewModel->title = "Signed out";
                } else {
                    $this->viewModel->try = true;
                    $this->viewModel->signout = false;
                    $this->viewModel->title = "Sign out";
                }

                $this->model->unload();
            } else {
                $this->viewModel->try = false;
                $this->viewModel->signout = false;
                $this->viewModel->title = "Sign out";
            }
        } else {
            Utility::redirect("user");
        }
    }

    public function changeAction($change)
    {
        if ($change == "email" || $change == "password") {
            if ($this->session->check("logged", true)) {
                $this->viewModel->title = "Change " . $change;
                $this->viewModel->what = $change;

                if ($this->post->request) {
                    if ($change == "email") {
                        if (isset($this->post->email) && isset($this->post->password)) {
                            $this->model->load("user");
                            $this->viewModel->error = (object) array();

                            if (!filter_var($this->post->email, FILTER_VALIDATE_EMAIL)) {
                                $this->viewModel->error->email = "Inval email format";
                            }
                            if (!$this->user->checkUser($this->session->nick, Utility::hash($this->post->password))) {
                                $this->viewModel->error->password = "Bad password";
                            }

                            if (empty((array) $this->viewModel->error)) {
                                if ($this->user->editEmail($this->session->nick, $this->post->email)) {
                                    $this->viewModel->change = true;
                                } else {
                                    $this->viewModel->change = false;
                                }
                            }

                            $this->model->unload();
                        }
                    } else {
                        if (isset($this->post->old_password) && isset($this->post->new_password) && isset($this->post->new_repeat_password)) {
                            $this->model->load("user");
                            $this->viewModel->error = (object) array();

                            if (!$this->user->checkUser($this->session->nick, Utility::hash($this->post->old_password))) {
                                $this->viewModel->error->old_password = "Bad password";
                            }
                            if (strlen($this->post->new_password) < 8 || strlen($this->post->new_password) > 64) {
                                $this->viewModel->error->new_password = "Length of password has to be between 5 and 32 characters";
                            }
                            if ($this->post->new_password != $this->post->new_repeat_password) {
                                $this->viewModel->error->new_repeat_password = "Passwords are not equal";
                            }

                            if (empty((array) $this->viewModel->error)) {
                                if ($this->user->editPassword($this->session->nick, Utility::hash($this->post->new_password))) {
                                    $this->viewModel->change = true;
                                } else {
                                    $this->viewModel->change = false;
                                }
                            }

                            $this->model->unload();
                        }
                    }
                }
            } else {
                Utility::redirect("login");
            }
        } else {
            Utility::redirect("error");
        }
    }
}
