<?php

namespace Core;

abstract class Controller
{
    public function __call($method, $args)
    {
        if (isset($this->$method)) {
            $func = $this->$method;
            return call_user_func_array($this->$method, $args);
        }
    }

    public function beforeRun($data, $action)
    {

    }

    public function afterRun()
    {

    }
}
