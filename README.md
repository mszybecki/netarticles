# NetArticles

## Description

Netarticles was my the biggest project in High School.  
Project had to contain main page and pages where you could add, modify and remove datas in database.  
I am an ambitious man so I decided to make something much more advanced.  
And that is the way which I created my little and fast framework written in php.

## Framework - beginning

At the beginning there was simple routing - "controller/method".  
Almost everything were in one file - logic, routing and much more.

## Framework - final

Nowadays it has advanced routing which is read from file.  
There is possibility of writing own addition which can be dependence from others.  
Views are written in Smarty.  
Some datas are injected to class for example in addition class is injected properly controller and module manager. 